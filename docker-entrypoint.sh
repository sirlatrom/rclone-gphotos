#!/bin/sh
set -eu
cp $HOME/rclone.conf $HOME/.config/rclone/rclone.conf
exec rclone "$@"
